FROM node:carbon
RUN npm install -g serve create-react-app
ARG REACT_APP_FACEBOOK_APP_ID
ARG REACT_APP_API_ENDPOINT
EXPOSE 5000
ENV NODE_ENV production
WORKDIR /webapp
ADD package.json .
RUN yarn install
ADD . .
RUN if [ -d "build/" ]; then rm -rf build; fi
RUN yarn run build
CMD serve -s -c 0 build
