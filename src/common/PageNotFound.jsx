import React from 'react'

class NotFound extends React.Component {
    
    componentWillMount = () => {
        this.props.handleTitle("")
        this.props.showSearchBar(false)
    }

    render() {
        return (<div>Page Not Found </div>)
    }
}

export default NotFound