import React from 'react';
import './App.css';

import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'
import Axios from "./common/AxiosConfiguration"

// Public Routes
import AppFrame from './frame/AppFrame'
import Inventory from './inventory/Inventory'

// Authenticated Route
import InventoryUsage from './usage/InventoryUsage'
import PageNotFound from './common/PageNotFound';

// ADMIN ROUTES
import UsageManagement from './usage/InventoryUsageManagementAdmin'
import UserManagement from './user/UserAdmin'
import InventoryManagement from './inventory/InventoryAdmin'

const PublicRoute = ({path, exact, component: Component, handleSearch, showSearchBar, setGlobalState, ...rest}) =>{
  return (
    <Route exact path={path} render={(props) => <Component {...props} showSearchBar={showSearchBar} handleSearch={handleSearch} setGlobalState={setGlobalState} {...rest} /> }/>
  )
}

const AuthenticatedRoute = ({component: Component, isAuthenticated, handleTitle, showSearchBar, ...rest}) => {
  if (isAuthenticated === undefined){
    return null
  }
  return (
    <Route
      render={(props) => isAuthenticated ? 
        <Component {...props} {...rest} handleTitle={handleTitle} showSearchBar={showSearchBar} /> 
      : 
        <Redirect to={{pathname: '/', state: {from: props.location}}} />}/>
  )
}

const AdministratorRoute = ({component: Component, isAuthenticated, role, handleTitle, ...rest}) => {
  if (isAuthenticated === undefined){
    return null
  }

  if (isAuthenticated === true || role === "ROLE_ADMIN"){
    return (
      <Route render={(props) => <Component {...props} {...rest} handleTitle={handleTitle} />}/>
    )
  } else {
    return (
      <Route render={(props) => <Redirect to={{pathname: '/', state: {from: props.location}}} />} />
    )
  }
}

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isAuthenticated: undefined,
      accessToken: undefined,
      expiresAt: undefined,
      name: undefined,
      picture: undefined,
      role: undefined,
      id: undefined,
      search: "",
      showSearch: true,
      title: "",
      snackBarOpen: false,
      snackMessage: ""
    }
  }

  handleSnackbarOpen = message => this.setState((state) => ({snackBarOpen: true, snackMessage: message}))
  handleSnackbarClose = () => this.setState({snackBarOpen: false})

  handleSearch = (search) => {
    this.setState({search})
  }

  showSearchBar = (showSearch) => {
    this.setState({showSearch, search: "" })
  }

  handleTitle = (title) => {
    this.setState({title})
  }

  updateState = (input) => new Promise((resolve) => {
    this.setState((oldState) => 
      Object.keys(input).reduce((tmp, val) => {
        if (val === "accessToken"){
          const accessToken = input[val]
          Axios.defaults.headers.common['Authorization'] = `Bearer ${accessToken}`
        }
        tmp[val] = input[val]
        return tmp 
      }, {})
    )
    return resolve()
  })

  render(){
    return (
      <Router>
        <div >
          <PublicRoute component={AppFrame} setGlobalState={this.updateState} handleSearch={this.handleSearch} {...this.state} handleSnackbarClose={this.handleSnackbarClose} />
          <div style={{paddingLeft: '2%', paddingRight: '2%', paddingTop: '1.5%',  paddingBottom: '1.5%'}}>
            <Switch>
              <PublicRoute exact path="/" component={Inventory} handleTitle={this.handleTitle} showSearchBar={this.showSearchBar} handleSnackbarOpen={this.handleSnackbarOpen}  {...this.state} />
              <AuthenticatedRoute path="/usage" component={InventoryUsage} handleTitle={this.handleTitle} showSearchBar={this.showSearchBar} {...this.state} handleSnackbarOpen={this.handleSnackbarOpen} />
              <AdministratorRoute path="/admin/user" {...this.state} component={UserManagement} showSearchBar={this.showSearchBar} handleTitle={this.handleTitle} handleSnackbarOpen={this.handleSnackbarOpen} />
              <AdministratorRoute path="/admin/usage" {...this.state} component={UsageManagement} showSearchBar={this.showSearchBar} handleTitle={this.handleTitle} handleSnackbarOpen={this.handleSnackbarOpen} />
              <AdministratorRoute path="/admin/inventory"  {...this.state} component={InventoryManagement} showSearchBar={this.showSearchBar} handleTitle={this.handleTitle} handleSnackbarOpen={this.handleSnackbarOpen} />
              <PublicRoute component={PageNotFound} showSearchBar={this.showSearchBar} />
            </Switch>
          </div>
        </div>
      </Router>
    )
  }
}

export default App;
