import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import 'typeface-roboto'
import App from "./App"

function Main(){
  return (
    <App />
  );
}

ReactDOM.render(<Main />, document.getElementById('root'));
registerServiceWorker();
