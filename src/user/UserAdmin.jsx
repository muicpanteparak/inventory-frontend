import React from 'react'

export default class UserAdmin extends React.Component {

    componentWillMount = () => {
        this.props.handleTitle("User Management")
        this.props.showSearchBar(false)
    }

    render(){
        return (<div>UserAdmin, ROLE_ADMIN ONLY</div>)
    }

}