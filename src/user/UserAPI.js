import Axios from '../common/AxiosConfiguration'

class User {
    whoami(fields = "-authorities") {
        return Axios.get(`/user/whoami?fields=${fields}`)
            .then(response => (response.data))
    }

    listUsers(fields = "*") {
        return Axios.get(`/user?fields=${fields}`)
            .then(response => (response.data))
    }
}

export default new User();