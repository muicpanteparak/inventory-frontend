import React from 'react'
import Grid from 'material-ui/Grid';
import Card, { CardActions, CardContent } from 'material-ui/Card';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import { CircularProgress } from 'material-ui/Progress';
import InventoryAPI from './InventoryAPI';
import Select from 'material-ui/Select';
import Input, { InputLabel } from 'material-ui/Input'

const styles = {
    card: { 
        width: "100%"
    },
    pos: {}
}

class InventoryItem extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            name: props.name,
            itemId: props.itemId,
            price: props.price,
            qty: 1,
        }
    }

    handleChange = (e) => {
        if (e.target.value > 0 || e.target.value === ""){
            this.setState({qty: Number(e.target.value.trim())})
        }
    }

    handleBuy = (e) => {
        if (this.props.isAuthenticated){
            InventoryAPI.buyItem(this.state.itemId, this.state.qty)
                .then(s => this.props.toggleSnackbar("Buy Succeeded"))
                .catch(e => this.props.toggleSnackbar("Buy Failed"))
        } else {
            this.props.toggleSnackbar("You're not log in")
        } 
    }

    render(){
        const {name, price, qty} = this.state

        return (
            <Card style={styles.card}>
                <CardContent>
                    <Typography type="headline" component="h2"> <span>{name}</span> </Typography>
                    <Typography style={styles.pos}>฿ {price}</Typography>
                </CardContent>
                <CardActions>
                    <InputLabel htmlFor="age-native-simple">Quantity</InputLabel>
                    <Select native value={qty} onChange={this.handleChange} input={<Input id="age-native-simple" />}>
                        {
                            Array.from(new Array(10),(val,index)=> index+1).map(val => {
                                return (<option value={val}>{val}</option>)
                            })
                        }
                    </Select>
                    <Button dense onClick={this.handleBuy}>Buy</Button>
                </CardActions>
            </Card>
        );
    }
}

class Inventory extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            data: [],
            loading: true
        }
    }

    componentWillMount = () => {
        this.props.handleTitle("Inventory")
        this.props.showSearchBar(true)
    }

    componentDidMount = () => InventoryAPI.listInventory().then(data => this.setState({data, loading: false}))

    searchFilter = (item) => {
        return (item.name.toLowerCase().indexOf(this.props.search.toLowerCase()) >= 0)
    }
    render() {
        const { data, loading } = this.state
        const { isAuthenticated, handleSnackbarOpen } = this.props

        if (loading) return (<div  align="center" style={{margin: '1.8% auto'}}><CircularProgress size={50} style={{margin: '10% auto'}}/> </div>)
        return (
            <div>
              <Grid container spacing={24} alignContent="center" style={{flexGrow: 1}}>
                    {data.map(item => this.searchFilter(item) && (
                        <Grid key={item.id} item xs={12} sm={6} md={4} lg={3}>
                            <InventoryItem name={item.name} itemId={item.id} price={item.price} toggleSnackbar={handleSnackbarOpen} isAuthenticated={isAuthenticated} />
                        </Grid>)
                    )}
              </Grid>
            </div>
          );
    }
}

export default Inventory;