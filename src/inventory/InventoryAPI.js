import axios from '../common/AxiosConfiguration'

class Inventory {
    listInventory = () => {
        return axios.get('/inventory?fields=-isShow')
            .then(response => (response.data))
    }

    createNewItem = (name, price, show) => {
        return axios.post('/inventory/create', {name, price, show}) 
            .then(response => (response.data))
    }

    searchById = (id) => {
        return axios.get(`/inventory/${id}`)
            .then(response => (response.data))
    }

    buyItem = (inventoryId, qty) => {
        return axios.post('/inventory/buy', {inventoryId, qty})
            .then(response => (response.data))
    }

    listAllInventory = () => {
        return axios.get('inventory/all')
            .then(({ data }) => data)
    }
}

export default new Inventory()