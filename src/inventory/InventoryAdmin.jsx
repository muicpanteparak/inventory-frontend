import React from 'react'
import InventoryAPI from './InventoryAPI'
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Table, {
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
} from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import Tooltip from 'material-ui/Tooltip';
import TimeAgo from 'react-timeago'
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import Dialog, {DialogTitle, DialogContent, DialogContentText, DialogActions} from 'material-ui/Dialog'
import AddIcon from 'material-ui-icons/Add';
import Switch from 'material-ui/Switch';
import { CircularProgress } from 'material-ui/Progress';

const styles = theme => ({
    root: {
      width: '100%',
    },
    table: {
      minWidth: 800,
    },
    tableWrapper: {
      overflowX: 'auto',
    },
    absolute: {
      position: 'fixed',
      bottom: theme.spacing.unit * 2,
      right: theme.spacing.unit * 2.5,
    },
    tableFooter: {
      paddingLeft: theme.spacing.unit * 2
    }
});

const columnData = [
    {id: 'id', numeric: true, disablePadding: false, label: "Inventory ID"},
    { id: 'name', numeric: false, disablePadding: false, label: 'Product Name' },
    { id: 'price', numeric: true, disablePadding: false, label: 'Price' },
    { id: 'show', numeric: false, disablePadding: false, label: 'Visible' },
    { id: 'createdBy', numeric: false, disablePadding: false, label: 'Created By' },
    { id: 'modifiedBy', numeric: false, disablePadding: false, label: 'Updated By' },
    { id: 'createdDate', numeric: false, disablePadding: false, label: 'Created At' },
    { id: 'modifiedDate', numeric: false, disablePadding: false, label: 'Updated At' }
  ];

  class AddItemDialog extends React.Component {
    constructor(props){
      super(props)
      this.state = {
        name: "",
        price: "",
        show: true
      }
    }
    render(){
      const {open, handleOpen, handleClose, classes, handleSubmit} = this.props
      const {name, price, show} = this.state
      return (
        <div style={{leftMargin: '20%', rightMargin: '20%'}}>
          <Tooltip title="Add New Item">
            <Button fab color="primary" aria-label="Add" className={classes.absolute} onClick={handleOpen} >
              <AddIcon />
            </Button>
          </Tooltip>
          <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Add Inventory</DialogTitle>
              <DialogContent>
                <DialogContentText>
                  <span>Add Inventory to the System</span>
                </DialogContentText>
                <TextField
                  autoFocus
                  margin="dense"
                  id="name"
                  label="Name"
                  type="text"
                  fullWidth
                  value={name}
                  onChange={(e) => this.setState({name: e.target.value})}
                />
                
                <TextField
                  margin="dense"
                  id="price"
                  label="Price"
                  type="number"
                  fullWidth
                  value={price}
                  onChange={(e) => this.setState({price: e.target.value})}
                />

              <Switch
                checked={this.state.show}
                onChange={(event, checked) => this.setState({ show: checked })}
              />      


              </DialogContent>
              <DialogActions>
                <Button color="primary" onClick={handleClose} >
                  Cancel
                </Button>
                <Button onClick={() => {handleSubmit(name, price, show); handleClose();}} color="primary">
                  Add
                </Button>
              </DialogActions>
          </Dialog>
        </div>
      );
    }
  }

class EnhancedTableHead extends React.Component {
    static propTypes = {
      onRequestSort: PropTypes.func.isRequired,
      order: PropTypes.string.isRequired,
      orderBy: PropTypes.string.isRequired,
      rowCount: PropTypes.number.isRequired,
    };
  
    createSortHandler = property => event => {
      this.props.onRequestSort(event, property);
    };
  
    render() {
      const {  order, orderBy } = this.props;
  
      return (
        <TableHead>
          <TableRow>
            {columnData.map(column => {
              return (
                <TableCell
                  key={column.id}
                  numeric={column.numeric}
                  padding={column.disablePadding ? 'none' : 'default'}
                  sortDirection={orderBy === column.id ? order : false}
                >
                  <Tooltip
                    title="Sort"
                    placement={column.numeric ? 'bottom-end' : 'bottom-start'}
                    enterDelay={300}
                  >
                    <TableSortLabel
                      active={orderBy === column.id}
                      direction={order}
                      onClick={this.createSortHandler(column.id)}
                    >
                      {column.label}
                    </TableSortLabel>
                  </Tooltip>
                </TableCell>
              );
            }, this)}
          </TableRow>
        </TableHead>
      );
    }
}
  

class InventoryAdmin extends React.Component {
  constructor(props){
      super(props)
      this.state = {
          data: [],
          order: 'asc',
          orderBy: 'id',
          page: 0,
          rowsPerPage: 5,
          name: "",
          price: "",
          addItemDialog: false,
          loading: true,
      }
  }

  componentWillMount = () => {
      this.props.handleTitle("Inventory Management")
      this.props.showSearchBar(true)
  }

  componentDidMount = () => {
      this.updateInventory()
  }

  updateInventory = () => InventoryAPI.listAllInventory().then(data => this.setState({loading: false, data: data.sort((a, b) => (b[this.state.orderBy] > a[this.state.orderBy] ? -1 : 1))}))

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    const data =
      order === 'desc'
        ? this.state.data.sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
        : this.state.data.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));

    this.setState({ data, order, orderBy });
  };

  handleChange = panel => (event, expanded) => this.setState({expanded: expanded ? panel : false});

  handleChangePage = (event, page) => this.setState({ page })

  handleChangeRowsPerPage = event => this.setState({ rowsPerPage: event.target.value });

  readableDate = (date) => {
    const d = new Date(date);
    return d.toLocaleDateString() + " " + d.toLocaleTimeString()
  }

  searchFilter = (data) => data.name.toLowerCase().indexOf(this.props.search.toLowerCase()) >= 0

  createInventory = (name, price, show) => {
    InventoryAPI.createNewItem(name, price, show).then(res => {
    }).catch(err => console.error(err))
  }

  onAddItemDialogClose = () => this.setState({ addItemDialog: false })

  render = () => {
      const { classes } = this.props;
      const { order, orderBy, data, page, rowsPerPage, addItemDialog, loading } = this.state;
      const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);
      if (loading) return (<div  align="center" style={{margin: '1.8% auto'}}><CircularProgress size={50} className={classes.circularProgress} /> </div>)
      return (
          <div>
            <AddItemDialog open={addItemDialog} handleSubmit={this.createInventory} handleClose={this.onAddItemDialogClose} classes={classes} handleOpen={() => this.setState({addItemDialog: true})}/>
            <Paper className={classes.root}>
                <div className={classes.tableWrapper}>
                    <Table className={classes.table}>
                        <EnhancedTableHead
                            order={order}
                            orderBy={orderBy}
                            onRequestSort={this.handleRequestSort}
                            rowCount={data.length}
                        />
                    <TableBody>
                    {data.filter(data => this.searchFilter(data)).slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(({id, name, price, show, createdBy, modifiedBy, createdDate, modifiedDate}) => 
                        (
                          <TableRow hover tabIndex={-1} key={id}>
                              <TableCell numeric>{id}</TableCell>
                              <TableCell>{name}</TableCell>
                              <TableCell numeric>{price}</TableCell>
                              <TableCell padding='default'>{show ? "show" : "hide"}</TableCell>
                              <TableCell>{createdBy}</TableCell>
                              <TableCell>{modifiedBy}</TableCell>
                              <TableCell>
                                <Tooltip title={this.readableDate(createdDate)} value={createdDate} enterDelay={300}>
                                  <TimeAgo title={null} date={createdDate} live={true} />
                                </Tooltip>
                              </TableCell>

                              <TableCell>
                                <Tooltip title={this.readableDate(modifiedDate)} value={modifiedDate} enterDelay={300}>
                                  <TimeAgo title={null} date={modifiedDate} live={true} />
                                </Tooltip>
                              </TableCell>
                          </TableRow>
                        ))}
                      {data.length > rowsPerPage && emptyRows > 0 && (
                        <TableRow style={{ height: 48 * emptyRows }}>
                          <TableCell colSpan={6} />
                        </TableRow>
                      )}
                    </TableBody>
                    <TableFooter>
                      <TableRow>
                        <TablePagination
                          count={data.length}
                          rowsPerPage={rowsPerPage}
                          page={page}
                          backIconButtonProps={{'aria-label': 'Previous Page'}}
                          nextIconButtonProps={{'aria-label': 'Next Page'}}
                          onChangePage={this.handleChangePage}
                          onChangeRowsPerPage={this.handleChangeRowsPerPage}
                          className={classes.tableFooter}
                        />
                      </TableRow>
                    </TableFooter>
                  </Table>
                </div>
            </Paper>
          </div>
      )
    }
}

InventoryAdmin.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(InventoryAdmin);