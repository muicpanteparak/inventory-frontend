import axios from '../common/AxiosConfiguration'
import UserAPI from '../user/UserAPI'
import _ from 'lodash'

class InventoryUsageAPI {
    listMyUsage = (fields = "id,createdDate,createdBy.displayname,inventory.name,qty,broughtPrice") => {
        return axios.get(`/inventory/usage?fields=${fields}`)
            .then((r) => r.data)
            .then(r => r.map(d =>  ({ 
                id: d.id, 
                createdDate: d.createdDate,
                createdBy: d.createdBy.displayname,
                name: d.inventory.name,
                qty: d.qty,
                price: d.broughtPrice,
                total: d.qty * d.broughtPrice
            })))
    }

    listAllUsage = () => {
        const items = axios.get("/inventory/usage/all")
        .then(r => (r.data))
        .then(data => data.reduce((acc, val) => {
            const {facebookId, displayname ,...other} = val
            if (!(facebookId in acc)) 
                acc[facebookId] = {displayname, items: []}
            
            acc[facebookId]["items"].push({...other})
            return acc;
        }, {}))

        const users = UserAPI.listUsers("facebookId,displayName")

        return Promise.all([items, users])
            .then(([first, second]) =>
                _.mergeWith(first, second.reduce((acc, {displayName: displayname, facebookId, ...other}) => {
                    acc[facebookId] = {displayname, ...other}
                    return acc
                }, {}), (val, src) => _.isArray(src.items) ? val.items : src.items)
            )
    }
}

export default new InventoryUsageAPI() 