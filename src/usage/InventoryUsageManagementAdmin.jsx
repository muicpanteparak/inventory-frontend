import React from 'react'
import InventoryUsageAPI from './InventoryUsageAPI'

export default class InventoryManagementAdmin extends React.Component {

    componentWillMount = () => {
        this.props.handleTitle("Usage Management")
        this.props.showSearchBar(false)
    }

    componentDidMount = () => {
        InventoryUsageAPI.listAllUsage()
            .then(data => console.log(data))
    }

    render(){
        return (<div>Inventory Usage Admin, ROLE_ADMIN ONLY</div>)
    }
}