import React from 'react'
import InventoryUsageAPI from './InventoryUsageAPI'
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Table, {
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
} from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import Card, {CardContent} from 'material-ui/Card';
import Typography from 'material-ui/Typography'
import Tooltip from 'material-ui/Tooltip';
import TimeAgo from 'react-timeago'
import { CircularProgress } from 'material-ui/Progress';
import _ from 'lodash';

const styles = theme => ({
    root: {
      width: '100%',
      marginBottom: "2%"
    },
    table: {
      minWidth: 800,
    },
    tableWrapper: {
      overflowX: 'auto',
    },
    circularProgress: {
      margin: '10% auto'
    },
    card: {
      minWidth: 275,
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      marginBottom: 16,
      fontSize: 14,
      color: theme.palette.text.secondary,
    },
    pos: {
      marginBottom: 12,
      color: theme.palette.text.secondary,
    },
});

const columnData = [
    { id: 'name', numeric: false, disablePadding: false, label: 'Product Name' },
    { id: 'price', numeric: true, disablePadding: false, label: 'Price' },
    { id: 'qty', numeric: true, disablePadding: false, label: 'Quantity' },
    { id: 'total', numeric: true, disablePadding: false, label: 'Total' },
    { id: 'createdDate', numeric: true, disablePadding: false, label: 'Transaction Date' },
  ];

class EnhancedTableHead extends React.Component {
    static propTypes = {
      onRequestSort: PropTypes.func.isRequired,
      order: PropTypes.string.isRequired,
      orderBy: PropTypes.string.isRequired,
      rowCount: PropTypes.number.isRequired,
    };
  
    createSortHandler = property => event => {
      this.props.onRequestSort(event, property);
    };
  
    render() {
      const {  order, orderBy} = this.props;
  
      return (
        <TableHead>
          <TableRow>
            {columnData.map(column => {
              return (
                <TableCell
                  key={column.id}
                  numeric={column.numeric}
                  padding={column.disablePadding ? 'none' : 'default'}
                  sortDirection={orderBy === column.id ? order : false}
                >
                  <Tooltip
                    title="Sort"
                    placement={column.numeric ? 'bottom-end' : 'bottom-start'}
                    enterDelay={300}
                  >
                    <TableSortLabel
                      active={orderBy === column.id}
                      direction={order}
                      onClick={this.createSortHandler(column.id)}
                    >
                      {column.label}
                    </TableSortLabel>
                  </Tooltip>
                </TableCell>
              );
            }, this)}
          </TableRow>
        </TableHead>
      );
    }
}
  

class InventoryUsage extends React.Component {
  constructor(props){
      super(props)
      this.state = {
          data: [],
          selected: [],
          order: 'desc',
          orderBy: 'createdDate',
          page: 0,
          rowsPerPage: 5,
          loading: true
      }
  }

  componentWillMount = () => {
      this.props.handleTitle("My Usage")
      this.props.showSearchBar(true)
  }

  componentDidMount = () => {
      InventoryUsageAPI.listMyUsage()
          .then(data => this.setState({loading: false, data: data.sort((a, b) => (b[this.state.orderBy] < a[this.state.orderBy] ? -1 : 1))}))

  }

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    const data =
      order === 'desc'
        ? this.state.data.sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
        : this.state.data.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));

    this.setState({ data, order, orderBy });
  };

  handleChange = panel => (event, expanded) => {
      this.setState({
        expanded: expanded ? panel : false,
      });
    };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  readableDate = (date) => {
    const d = new Date(date);
    return d.toLocaleDateString() + " " + d.toLocaleTimeString()
  }

  searchFilter = (data) => {
    return data.name.toLowerCase().indexOf(this.props.search.toLowerCase()) >= 0
  }
  render = () => {
      const { classes } = this.props;
      const { order, orderBy, data, page, rowsPerPage, loading } = this.state;
      const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);

      if (loading) return (<div  align="center" style={{margin: '1.8% auto'}}><CircularProgress size={50} className={classes.circularProgress} /> </div>)
      return (
        <div>
          <Paper className={classes.root}>
          <Card className={classes.card}>
            <CardContent>
              <Typography className={classes.title}>Usage Summary</Typography>
              <Typography component="p">
                Total price: {_.sum(data.map(({ total }) => total))}
              </Typography>
            </CardContent>
          </Card>
          </Paper>
          <Paper className={classes.root}>
            <div className={classes.tableWrapper}>
              <Table className={classes.table}>
                      <EnhancedTableHead
                          order={order}
                          orderBy={orderBy}
                          onRequestSort={this.handleRequestSort}
                          rowCount={data.length}
                      />
                  <TableBody>
                    {data.filter(data => this.searchFilter(data)).slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(({ name, price, qty, total, id, createdDate}) => 
                      (
                        <TableRow hover tabIndex={-1} key={id}>
                            <TableCell padding="default">{name}</TableCell>
                            <TableCell numeric>{price}</TableCell>
                            <TableCell numeric>{qty}</TableCell>
                            <TableCell numeric>{total}</TableCell>
                            <TableCell numeric>
                              <Tooltip title={this.readableDate(createdDate)} value={createdDate} enterDelay={300}>
                                <TimeAgo title={null} date={createdDate} live={true} />
                              </Tooltip>
                            </TableCell>
                        </TableRow>
                      ))}
                    {data.length > rowsPerPage && emptyRows > 0 && (
                      <TableRow style={{ height: 48 * emptyRows }}>
                        <TableCell colSpan={6} />
                      </TableRow>
                    )}
                  </TableBody> 
                  <TableFooter>
                    <TableRow>
                      <TablePagination
                        count={data.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        backIconButtonProps={{'aria-label': 'Previous Page'}}
                        nextIconButtonProps={{'aria-label': 'Next Page'}}
                        onChangePage={this.handleChangePage}
                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                      />
                    </TableRow>
                  </TableFooter>
                </Table>
              </div>
          </Paper>
          </div>
      )
  }
}

InventoryUsage.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(InventoryUsage);