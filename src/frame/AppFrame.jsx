import React from 'react'
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import Drawer from 'material-ui/Drawer';
import ChevronLeftIcon from 'material-ui-icons/ChevronLeft';
import { ViewModule, Done } from 'material-ui-icons'
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import Divider from 'material-ui/Divider';
import ListSubheader from 'material-ui/List/ListSubheader';
import User from '../user/UserAPI'
import AppSearch from './AppSearch'
import ClickAwayListener from 'material-ui/utils/ClickAwayListener';
import SupervisorAccount from 'material-ui-icons/SupervisorAccount';
import LoginComponent from './LoginComponent'
import Snackbar from 'material-ui/Snackbar'

const drawerWidth = 350

const styles = {
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    flex: {
        flex: 1,
    },
    root: {
        width: '100%',
        marginBottom: 60
    },
    drawer: {
        width: '100%',
        maxWidth: drawerWidth,
        position: 'relative',
        height: '100%'
    }
}

const menu = [
    {name: "Inventory", path: "/", icon: ViewModule},
    {name: "My Usage", path: "/usage", icon: Done}
]

const adminMenu = [
    {name: "Inventory Management", path: "/admin/inventory", icon: ViewModule},
    {name: "Usage Management", path: "/admin/usage", icon: Done},
    {name: "User Management", path: "/admin/user", icon: SupervisorAccount}
]

const followRoute = (path, location, history, handleDrawerClose) => {
    if (path !== location){
        history.push(path)
    }
    handleDrawerClose()
}

const SideDrawer = (props) => {
    const {role, open, handleDrawerClose, history, location} = props
    if (role === undefined){
        return null
    }

    // TODO add main div, and push content when side nav is open
    return (
        <ClickAwayListener onClickAway={(e) => handleDrawerClose()}>
            <Drawer type="persistent" open={open}>
                <div style={styles.drawer}>
                    <div style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-end', padding: '0 16px', height: 60, maxHeight: 60}}>
                        <IconButton onClick={handleDrawerClose}>
                            <ChevronLeftIcon /> 
                        </IconButton>
                    </div>
                    <Divider />
                    <div>
                        <List style={styles.drawer} subheader={<ListSubheader>Dashboard</ListSubheader>}>
                        {
                            menu.map(({name, path, icon: Icon}) => (
                                <ListItem key={name} button onClick={(e) => followRoute(path, location.pathname, history, handleDrawerClose)}>
                                    <ListItemIcon>
                                        <Icon />
                                    </ListItemIcon>
                                    <ListItemText inset primary={name}/>
                                </ListItem>

                            ))
                        }
                        </List>
                        {
                            role === "ROLE_ADMIN" && 
                                <div>
                                    <Divider />
                                    <List style={styles.drawer} subheader={<ListSubheader>Administrator Dashboard</ListSubheader>}>
                                    {
                                        adminMenu.map(({name, path, icon: Icon}) => (
                                            <ListItem key={name} button onClick={(e) => followRoute(path, location.pathname, history, handleDrawerClose)}>
                                                <ListItemIcon>
                                                    <Icon />
                                                </ListItemIcon>
                                                <ListItemText inset primary={name}/>
                                            </ListItem>
                                        ))
                                    }
                                    </List>
                                </div>
                        }
                    </div>
                </div>
            </Drawer>
        </ClickAwayListener>
    )
}

class AppFrame extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            open: false
        }
    }

    handleSocialLoginSuccess = (user) => {
        this.props.setGlobalState(
            {
                accessToken: user.token.accessToken, 
                expiresAt: new Date(user.token.expiresAt),
                name: user.profile.name,
                id: user.profile.id,
                picture: user.profile.profilePicURL
            })
            .then(() => User.whoami())
            .then(data => this.props.setGlobalState({
                isAuthenticated: true, 
                role: data.role
            }))
            .catch(err => this.props.setGlobalState({ isAuthenticated: false, role: undefined, expiresAt: 0, name: undefined, id: undefined, picture: undefined })
        )
    }
       
    handleSocialLoginFailure = (err) => {
        this.props.setGlobalState({ isAuthenticated: false, role: undefined, expiresAt: 0, name: undefined, id: undefined, picture: undefined })
    }  

    handleDrawerOpen = () => {
        setTimeout(() => this.setState({ open: true }), 270)
    }
    handleDrawerClose = () => {
        this.setState({ open: false })
    }
    handleChangeAnchor = event => {
        this.setState({ anchor: event.target.value})
    }
    

    render() {
        const { isAuthenticated, expiresAt, name, picture, search, handleSearch, children, title, showSearch, setGlobalState, snackBarOpen, snackMessage, handleSnackbarClose  } = this.props
        const { open } = this.state
        return (
            <div className='container' style={styles.root}>
                <SideDrawer {...this.props} open={open} handleDrawerClose={this.handleDrawerClose}/>
                
                <AppBar position="fixed" style={{height: 60}}>
                    <Toolbar>
                        <IconButton style={styles.menuButton} color="contrast" aria-label="Menu" onClick={this.handleDrawerOpen}>
                            <MenuIcon />
                        </IconButton>
                        <Typography type="title" color="inherit" style={styles.flex}>
                            {title}
                        </Typography>
                        {showSearch && <AppSearch id="search" value={search} onChange={handleSearch} />}
                        <LoginComponent setGlobalState={setGlobalState} authenticated={isAuthenticated} expires={expiresAt} picture={picture} name={name} handleSocialLoginSuccess={this.handleSocialLoginSuccess} handleSocialLoginFailure={this.handleSocialLoginFailure} />
                    </Toolbar>
                </AppBar>
                {children}
                <Snackbar
                    autoHideDuration={3000}
                    open={snackBarOpen} onClose={handleSnackbarClose}
                    message={<span>{snackMessage}</span>} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                />
            </div>
        )
    }
}
export default AppFrame