import React from 'react'
import Chip from 'material-ui/Chip'
import Avatar from 'material-ui/Avatar';
import FacebookSocialButton from './FacebookSocialButton'

const LoginSuccessComponent = ({picture, name, onClick}) => {
    return (<Chip avatar={<Avatar src={picture} />} label={name} onClick={onClick}/>)
}

class LoginComponent extends React.Component {
    componentDidMount(){
        this.interval = setInterval(this.areWeThereYet, 120000);
    }

    componentWillUnmount(){
        clearInterval(this.interval);
    }

    areWeThereYet = () => {
        if (this.props.authenticated){
            const offset = new Date(this.props.expires)
            offset.setMinutes(this.props.expires.getMinutes() - 5)
            if (offset <= new Date()){
                // console.log("Login Triggered")

                // Refresh Component
                this.props.setGlobalState({isAuthenticated: false})
                .then(() => this.props.setGlobalState({isAuthenticated: true}))
            }
        }
    }

    render(){
        const {picture, name, handleSocialLoginSuccess, handleSocialLoginFailure, authenticated} = this.props
        return (
            authenticated ? 
                <LoginSuccessComponent picture={picture} name={name} />
            :
                <FacebookSocialButton provider='facebook' appId={133674893886618} onLoginSuccess={handleSocialLoginSuccess} onLoginFailure={handleSocialLoginFailure} autoLogin={true}>
                    Login with Facebook
                </FacebookSocialButton>
        )
    }
}

export default LoginComponent