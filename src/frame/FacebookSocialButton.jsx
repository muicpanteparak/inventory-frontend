import React from 'react'
import SocialLogin from 'react-social-login'
import Button from 'material-ui/Button';

const LoginComponent = ({ children, triggerLogin, triggerLogout, onLoginSuccessComponent, ...props }) => {
  return (
    <Button color="contrast" onClick={triggerLogin} {...props}>
      { children }
    </Button>
  )
}
 
export default SocialLogin(LoginComponent)